import { SolidNodeClient } from "solid-node-client";
import { readFileSync } from "fs";
import pkg from "jsonld";
const { compact, fromRDF } = pkg;
import * as solid from "./solid.js";
import express from "express";
const app = express();

// Log in to pod
const credentials = JSON.parse(readFileSync("./source-credentials.json"));
const client = new SolidNodeClient();
let session = await client.login(credentials);

if (!session.isLoggedIn) throw new Error("Could not login as source");
console.log("Logged in as: ", session.webId);

const wellKnown = "http://localhost:3000/.well-known/solid";
const gateway = "http://localhost:3000/gateway";
const subscription = "http://localhost:3000/subscription";

const { data } = await solid.getFile(session, wellKnown);
console.log("data: ", { data });

const gwdata = {
  "@context": ["https://www.w3.org/ns/solid/notification/v1"],
  type: ["WebHookSubscription2021"],
  features: ["state"],
};
const gwretval = await solid.postFile(
  session,
  gateway,
  JSON.stringify(gwdata),
  "application/json"
);
console.log("data: ", { gwretval });

const subscrdata = {
  "@context": ["https://www.w3.org/ns/solid/notification/v1"],
  type: "WebHookSubscription2021",
  topic: "http://localhost:3000/source/foo/baz",
  target: "http://localhost:9999/webhook",
};

app.use(express.json());
app.post("/webhook", (req, res) => {
    const notification = req.body;
    console.log("notfication: ", notification);
    res.sendStatus(200);
});

app.listen(9999);

const subscrval = await solid.postFile(
  session,
  subscription,
  JSON.stringify(subscrdata),
  "application/json"
);
console.log("data: ", { subscrval });
