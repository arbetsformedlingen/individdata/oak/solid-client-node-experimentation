import { SolidNodeClient } from 'solid-node-client';
import { signedFile, sourceUrl } from './constants.js';
import { readFileSync } from 'fs';
import * as vc from './vc.js';
import * as solid from './solid.js';
import * as zlib from 'zlib';

const context = {
    id: "@id",
    type: "@type",
    status: "schema:status",
    subject: "http://purl.org/dc/terms/subject",
    name: "schema:name",
    familyName: "schema:familyName",
    givenName: "schema:givenName",
    identifier: "schema:idenifier",
    statusChangedDate: "schema:statusChangedDate",
    issued: "schema:issued",
    issuer: "schema:creator",
    Person: "schema:Person",
    GovernmentOrganization: "schema:GovernmentOrganization",
    UnemploymentStatus: "schema:DigitalDocument",
    schema: "http://schema.org/"
};

const credentialSubject = {
    type: "UnemploymentStatus",
    status: "unemployed",
    subject: "870820-5441",
    statusChangedDate: "2021-09-17",
    issuer: {
      type: "GovernmentOrganization",
      identifier: "202100-2114",
      name: "Arbetsförmedlingen"
    }
};

// Log in to pod
const credentials = JSON.parse(readFileSync('./source-credentials.json'));
const client = new SolidNodeClient();
const session = await client.login(credentials);

if(!session.isLoggedIn) throw new Error("Could not login as source");
console.log("Logged in as: ", session.webId);

// Write the key
const key = await vc.loadKey("source-key.pem", {id: `${sourceUrl}/key`, controller: `${sourceUrl}/controller`});

//console.log("key:", JSON.stringify(vc.publicKeyDoc(key), null, 2));
//console.log("controller:", JSON.stringify(vc.controllerDoc(key), null, 2));

await solid.putPublicJsonld(session, key.id, vc.publicKeyDoc(key));
await solid.putPublicJsonld(session, key.controller, vc.controllerDoc(key));

// Sign and upload document
const credential = vc.createCredential(context, credentialSubject);
console.log("credential: ", credential);

const signedJSON = await vc.issue(key, credential);
const signed = JSON.stringify(signedJSON, null, 0);
console.log("len signed: ", signed.length);

const deflatedVc = zlib.deflateSync(signed).toString('base64');
console.log("len deflatedVc: ", deflatedVc.length);

const jsonld = {
    "@context": [
        {
            "schema": "http://schema.org/"
        }
    ],
    "@type": "schema:Document"
  };

  jsonld["@id"] = "http://localhost:3000/public/vc";
  jsonld["schema:text"] = deflatedVc;

await solid.putJsonld(session, signedFile, jsonld);

/// Logout
await session.logout();
// even after logout the session still has timers that stop node from exiting.
process.exit(0);
