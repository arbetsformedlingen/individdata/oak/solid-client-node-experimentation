import { SolidNodeClient } from 'solid-node-client';
import SolidFileClient from 'solid-file-client';
import { readFileSync } from 'fs';
import pkg from 'jsonld';
const { compact, fromRDF } = pkg;
import { exit } from 'process';

// Log in to pod
const credentials = JSON.parse(readFileSync('./source-credentials.json'));
const client = new SolidNodeClient();
let session = await client.login(credentials);

if(!session.isLoggedIn) throw new Error("Could not login as source");
console.log("Logged in as: ", session.webId);


const fileClient = new SolidFileClient(client);
const key = await fileClient.readFile("http://localhost:3000/source/key.json", { headers: { Accept: 'application/n-quads' } });
console.log("content: ", key);
const doc = await fromRDF(key, {format: 'application/n-quads'});
const context = ["https://w3id.org/security/suites/ed25519-2018/v1"];
const compacted = await compact(doc, context);
console.log(JSON.stringify(compacted, null, 2));

exit(0);
//await fileClient.deleteFile("http://localhost:3000/public/obama.jsonld");
//const data = readFileSync("./obama.jsonld", "utf8");
//await fileClient.createFile("http://localhost:3000/public/obama.jsonld", data, 'application/ld+json');
const vc1 = readFileSync("./vc2.jsonld", "utf8");
await fileClient.createFile("http://localhost:3000/public/vc2.jsonld", vc1, 'application/ld+json');
//await fileClient.deleteFile("http://localhost:3000/public/vc1.jsonld");

//let content = await fileClient.readFile("http://localhost:3000/user/share/.acl", { headers: { Accept: 'application/ld+json' } });
//let content = await fileClient.readFile("http://localhost:3000/public/obama.jsonld", { headers: { Accept: 'text/turtle' } });
let content = await fileClient.readFile("http://localhost:3000/public/vc2.jsonld", { headers: { Accept: 'text/turtle' } });
console.log("content:", content);


/// Logout
await session.logout();
// even after logout the session still has timers that stop node from exiting.
process.exit(0);
