import { SolidNodeClient } from 'solid-node-client';
import { baseUrl } from './constants.js';
import { readFileSync } from 'fs';
import * as solid from './solid.js';

const addData = async (session, resource, data) => {
  console.log("Add data to: ", resource);
  const data = readFileSync(data, "utf8");
  await solid.putFile(session, resource, data, 'text/turtle');
}

const dataRequestsPath = (agent, resource) => `${baseUrl}/${agent}/data-requests/${resource}`;

// Log in to pod
const credentials = JSON.parse(readFileSync('./sink-credentials.json'));
const client = new SolidNodeClient();
let session = await client.login(credentials);

if(!session.isLoggedIn) throw new Error("Could not login as source");
console.log("Logged in as: ", session.webId);


addData(session,
  dataRequestsPath("sink", "e6cfe2c2-fa85-4c88-b330-31e593b8c7ec"),
  "./data-requests/e6cfe2c2-fa85-4c88-b330-31e593b8c7ec.ttl"
  );

addData(session,
  dataRequestsPath("user", "e6cfe2c2-fa85-4c88-b330-31e593b8c7ec"),
  "./data-requests/e6cfe2c2-fa85-4c88-b330-31e593b8c7ec-link.ttl"
  );
  
// Logout
await session.logout();
// even after logout the session still has timers that stop node from exiting.
process.exit(0);
