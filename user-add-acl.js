// Generate the acl records for a user based on the baseUrl
// defined in costants.js
import { baseUrl } from './constants.js';

const acl = `
@prefix acl: <http://www.w3.org/ns/auth/acl#>.
@prefix foaf: <http://xmlns.com/foaf/0.1/>.

<#source>
    a acl:Authorization;
    acl:agent <${baseUrl}/source/profile/card#me>;
    acl:accessTo <./>;
    acl:default <./>;
    acl:mode acl:Write.

<#sink>
    a acl:Authorization;
    acl:agent <${baseUrl}/sink/profile/card#me>;
    acl:accessTo <./>;
    acl:default <./>;
    acl:mode acl:Read.

<#owner>
    a acl:Authorization;
    acl:agent <${baseUrl}/user/profile/card#me>;
    acl:accessTo <./>;
    acl:default <./>;
    acl:mode acl:Read, acl:Write, acl:Control.
`

console.log(acl);
