import { sourceUrl } from './constants.js';
import * as vc from './vc.js';
import * as solid from './solid.js';
import * as zlib from 'zlib';

// import { SolidNodeClient } from 'solid-node-client';
import { readFileSync } from 'fs';


// This script can be invoked from Python with
// "subprocess.check_output(['node', '<correct_path>/issue-vc.js'], cwd='<correct_path>')"

// TODO: get passed parameters and issue credentials with correct data

const pnr = process.argv[2];
const solidGuid = process.argv[3];

const context = {
    id: "@id",
    type: "@type",
    status: "schema:status",
    subject: "http://purl.org/dc/terms/subject",
    name: "schema:name",
    familyName: "schema:familyName",
    givenName: "schema:givenName",
    identifier: "schema:idenifier",
    statusChangedDate: "schema:statusChangedDate",
    employer: "schema:name",
    issued: "schema:issued",
    issuer: "schema:creator",
    Person: "schema:Person",
    GovernmentOrganization: "schema:GovernmentOrganization",
    UnemploymentStatus: "schema:DigitalDocument",
    schema: "http://schema.org/"
};

const credentialSubject = {
    type: "UnemploymentStatus",
    status: "unemployed",
    subject: pnr,
    statusChangedDate: "2021-09-17",
    employer: "Bolaget AB",
    issuer: {
      type: "GovernmentOrganization",
      identifier: "202100-2114",
      name: "Arbetsförmedlingen"
    }
};

// Load the key
const key = await vc.loadKey("source-key.pem", {id: `${sourceUrl}/key`, controller: `${sourceUrl}/controller`});

// Log in to pod
// const credentials = JSON.parse(readFileSync('./source-credentials.json'));
// const client = new SolidNodeClient();
// const session = await client.login(credentials);

// await solid.putPublicJsonld(session, key.id, vc.publicKeyDoc(key));
// await solid.putPublicJsonld(session, key.controller, vc.controllerDoc(key));

// Sign and deflate
const credential = vc.createCredential(context, credentialSubject);
const signedJSON = await vc.issue(key, credential);
const signed = JSON.stringify(signedJSON, null, 0);
const deflatedVc = zlib.deflateSync(signed).toString('base64');

const jsonld = {
    "@context": [
        {
            "schema": "http://schema.org/"
        }
    ],
    "@type": "schema:Document"
  };

jsonld["@id"] = `http://localhost:3000/user/oak/responses/response-${solidGuid}`;
jsonld["urn:id"] = solidGuid;
jsonld["schema:text"] = signed;

const output = {vc: signed, deflated: deflatedVc, jsonld: jsonld};
const outputJSON = JSON.stringify(output, null, 0);

console.log(outputJSON);

// even after logout the session still has timers that stop node from exiting.
process.exit(0);
