import { SolidNodeClient } from 'solid-node-client';
import { baseUrl } from './constants.js';
import { readFileSync } from 'fs';
import * as solid from './solid.js';

const usage = `
>node sharepermissions [add | del]
    add - add /user/share/.acl
    del - delete /user/share/.acl
`;

if (process.argv.length < 3) {
    console.error(usage);
    process.exit(1);
}

const add = process.argv[2] === 'add';

// Log in to pod
const credentials = JSON.parse(readFileSync('./user-credentials.json'));
const client = new SolidNodeClient();
let session = await client.login(credentials);

if(!session.isLoggedIn) throw new Error("Could not login as user");
console.log("Logged in as: ", session.webId);

const aclPath = `${baseUrl}/user/share/.acl`;
console.log("aclPath: ", aclPath);

if (add) {
    // Add .acl to the shared directory
    const data = readFileSync("./acl.ttl", "utf8");
    await solid.putFile(session, aclPath, data, 'text/turtle');
} else {
    // Delete .acl from the shared directory
    await solid.deleteFile(session, aclPath);   
}


/// Logout
await session.logout();
// even after logout the session still has timers that stop node from exiting.
process.exit(0);
