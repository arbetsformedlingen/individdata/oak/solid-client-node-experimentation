import { SolidNodeClient } from 'solid-node-client';
import { signedFile } from './constants.js';
import { readFileSync } from 'fs';
import * as vc from './vc.js';
import * as solid from './solid.js';
import * as zlib from 'zlib';
import jsonld from 'jsonld';

// Log in to pod
const credentials = JSON.parse(readFileSync('./sink-credentials.json'));
const client = new SolidNodeClient();
let session = await client.login(credentials);

if(!session.isLoggedIn) throw new Error("Could not login as source");
console.log("Logged in as: ", session.webId);

// Read document and verify document
const document = await solid.getJsonld(session, signedFile);
//console.log("Document:");
//console.dir(document, { depth: null });
const compacted = await jsonld.compact(document, {});
//console.log("Compacted:");
//console.dir(compacted, { depth: null });
const deflatedVc = compacted["http://schema.org/text"];
const inflatedVc = zlib.inflateSync(new Buffer.from(deflatedVc, 'base64')).toString();
const vcJSON = JSON.parse(inflatedVc);
console.log("Verifiable credential:");
console.dir(vcJSON, { depth: null });

const docLoader = vc.documentLoaderGenerator(session);
const verified = await vc.verify(vcJSON, docLoader);
// TODO: Here we could verify the public key with a CA
console.log("Verification result:");
console.dir(verified, { depth: null });

// Logout
await session.logout();
// even after logout the session still has timers that stop node from exiting.
process.exit(0);
